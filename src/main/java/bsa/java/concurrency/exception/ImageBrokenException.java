package bsa.java.concurrency.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The image is broken.")
@Slf4j
public class ImageBrokenException extends RuntimeException {
    public ImageBrokenException() {
        super("Broken image!");
        log.error("Broken image!");
    }
}
